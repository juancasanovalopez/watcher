import sys
#import time
import telepot
#import os
import sqlite3
import qrcode

from time import sleep
from os import system
from data import *

#----------------------------------------------------------------------
#                           DEVELOPMENT GUIDE
#----------------------------------------------------------------------
#TODO: update_mode
#TODO: import solo lo que necesitas
#TODO: docu! <- docu!
#TODO: make it private
#TODO: sqlite a RAM con stringIO cargar en cada nueva ejecucion
#TODO: meteo function
#TODO: semver report de update
#TODO: WiFi request
#TODO: ssh "dancing" port function ;)
#----------------------------------------------------------------------
#DONE: :D
#----------------------------------------------------------------------


"""
conn = sqlite3.connect('data.db')
conn.execute('''    CREATE TABLE IF NOT EXISTS sw_data
                        (   token,  dev_path,   main_path,  setup_mode, prog_name,
                            service_name,   git_url,    script_name);''')
conn.execute('''    CREATE TABLE IF NOT EXISTS sw_version
                        (major, minor, patch);''')

conn.commit()
"""

sw_version = {
    "major":0,
    "minor":0,
    "patch":3
}

def make_sh_script(data):
    # Open a file
    script = open(data["script_name"], "w")
    content = "#!/bin/sh \n "
    content += " sudo rm -r "+data["dev_path"]
    content += " git clone "+data["git_url"]+"\n"
    content += " sudo systemctl stop "+data["service_name"]+".service \n"
    content += "cd " +data["dev_path"]+ "\n"
    #content += " rm "+data["main_path"]+data["prog_name"]+".py \n"

    content += " cp "+data["prog_name"]+".py "+data["main_path"]+" \n"
    content += " sudo systemctl start "+data["service_name"]+".service"+"\n"
    content += " sudo rm -r "+data["dev_path"]
    script.write(content)
    script.close()
    return 0;

def handle(msg):
    chat_id = msg['chat']['id']
    command = msg['text']

    #print('Got command: %s' % command)

    if command == 'hola':
        msg = 'Espabila ya no estas en Matrix! \U0001f430'
        bot.sendMessage(chat_id, msg)

    elif command == 'about':
        version = 'v'
        version += str(sw_version["major"])+'.'
        version += str(sw_version["minor"])+'.'
        version += str(sw_version["patch"]

        bot.sendMessage(chat_id, version)

    elif command =='buenas':
        bot.sendMessage(chat_id, 'buenas Tardes')

    elif command =='update':
        # crear lista de paquetes de python
        # crear script de instalacion
        # check version to install
        bot.sendMessage(chat_id, '**Installing... 0**')

        make_sh_script(sw_data)
        bot.sendMessage(chat_id, 'Installing... 1')
        system("cd /home/pi/watcher_dev/")
        bot.sendMessage(chat_id, 'Installing... 2')
        system("chmod +x update.sh")
        bot.sendMessage(chat_id, 'Installing... 3')
        system("./update.sh")
        #os.system("rm update.sh")


    elif command == '\U0001f430' :

        bin_lst = []
        for byte in bytes:
            o_z = format(byte,'08b')    # to binary
            bin_lst.append(str(o_z))    # add to list
        answer = ''.join(bin_lst)

        img = qrcode.make(answer)
        # ¿mejor png? ¿mejor jpg?
        img.save("code.jpg")

        #   bot.sendMessage(chat_id, answer)
        bot.sendPhoto(  chat_id,
                        open('code.jpg','rb'),
                        caption=' ')
        #os.system('rm code.jpg')
        #bot.sendPhoto(chat_id, 'code.jpg')

bot = telepot.Bot(sw_data["TOKEN"])
bot.message_loop(handle)


while 1:

    try:
        #time.sleep(10)
        sleep(10)
    except KeyboardInterrupt:
        print('\n Program interrupted')
        #GPIO.cleanup()
        exit()

    except:
        print('Other error or exception occured!')
        #GPIO.cleanup()

            #https://www.fnmoc.navy.mil/wxmap_cgi/cgi-bin/wxmap_single.cgi?area=nvg_europe2&dtg=2019021412&prod=prp&tau=000
            #https://www.fnmoc.navy.mil/wxmap_cgi/cgi-bin/wxmap_single.cgi?area=nvg_europe2&dtg=2019021412&prod=prp&tau=090a
